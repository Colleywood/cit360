public class Dessert {

    private String dessertName;
    private int dessertServings;

    public Dessert(String dessertName, int dessertServings) {

        this.dessertName = dessertName;
        this.dessertServings = dessertServings;
    }

    public String getDessertName() {
        return dessertName;
    }

    public void setDessertName(String dessertName) {
        this.dessertName = dessertName;
    }

    public int getDessertServings() {
        return dessertServings;
    }

    public void setDessertServings(int dessertServings) {
        this.dessertServings = dessertServings;
    }

    public String[] getDessertSampler() {
        return new String[0];
    }
}
