import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.sql.SQLOutput;

class DessertTest {

    @Test
    void getDessertName() {
        Dessert d = new Dessert("Jello", 2 );
        Assertions.assertEquals("Jello", d.getDessertName());
    }

    @Test
    void getDessertServings() {
        Dessert d = new Dessert("Jello", 3 );
        Assertions.assertEquals(d.getDessertServings(), 2);
    }

    @Test
    void getDessertSampler() {
        String[] expectedResult = {"Jello", "Cobbler", "Cake"};
        String[] actualResult = {"Jello", "Cobbler", "Cake"};
//        String[] actualResult = {"Cake", "Cobbler", "Jello"};
        Assertions.assertArrayEquals(expectedResult, actualResult);
    }


    public class Even {

        public boolean isEven (int number) {
            boolean results = false;
            if (number%2 == 0) {
                results = true;
            }
            return results;
        }
    }

    @Test
    void getDessertEven() {
        Even test = new Even();
        Assertions.assertTrue(test.isEven(4));
//        Assertions.assertTrue(test.isEven(5));
//        Assertions.assertFalse(test.isEven(4));
//        Assertions.assertFalse(test.isEven(5));

    }

    @Test
    void getSameDessert() {
        Assertions.assertSame(4,4);
//        Assertions.assertSame(3,4);
//        Assertions.assertSame("Bob", "Bob");
//        Assertions.assertSame("Bob", "bob");
//        Assertions.assertSame("Bob", "Robert");

    }
}