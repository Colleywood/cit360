package com.newland.myMovieData;

import javax.persistence.*;

@Entity
@Table(name = "Movie")
class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "tmdb_id")
    private String tmdbId;

    @Column(name = "last_watched")
    private String lastWatched;

    @Column(name = "times_watched")
    private int timesWatched;

    public Movie() {
        this.lastWatched = "";
        this.timesWatched = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getLastWatched() {
        return lastWatched;
    }

    public void setLastWatched(String lastWatched) {
        this.lastWatched = lastWatched;
    }

    public int getTimesWatched() {
        return timesWatched;
    }

    public void setTimesWatched(int timesWatched) {
        this.timesWatched = timesWatched;
    }

}
