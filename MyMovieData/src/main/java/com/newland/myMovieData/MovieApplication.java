package com.newland.myMovieData;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class MovieApplication extends Application {

}