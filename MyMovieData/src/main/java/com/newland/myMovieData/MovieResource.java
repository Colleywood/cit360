package com.newland.myMovieData;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/movie-servlet")
public class MovieResource {
    @GET
    @Produces("text/plain")
    public String movie() {

        return "Welcome!";
    }
}