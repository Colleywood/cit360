package com.newland.myMovieData;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.hibernate.Session;
import org.hibernate.query.Query;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.*;
import javax.servlet.annotation.*;


@WebServlet(name = "movie-servlet", urlPatterns =  {"/movie-servlet"} )
public class MovieServlet extends HttpServlet {
    private String tmdbApiKey;
    private String tmdbApiUrl;


    public void init() {
        tmdbApiKey = "12a9b3a8b65e18a6a943db0e966232bf";
        tmdbApiUrl = "https://api.themoviedb.org/3/";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        out.println("<!DOCTYPE html>");
        out.println("<html lang='en'>");
        out.println("<head>");
        out.println("<meta charset='UTF-8'>");
        out.println("<title>My Movie Database</title>");
        out.println("</head>");
        out.println("<body style='background-color:mediumTurquoise;'>");
        out.println("<form action='movie-servlet' method='post'>");
        out.println("<h1 style='text-align: center'>Let us help you find a great movie!</h1>");
        out.println("<img src='https://www.themoviedb.org/assets/2/v4/logos/v2/blue_square_2-d537fb228cf3ded904ef09b136fe3fec72548ebc1fea3fbbd1ad9e36364db38b.svg' alt='The Movie Database " +
                "Logo' " + " style='Float:right' width='115' height='85'>");
        out.println("<br><br>");
        out.println("<label for='pickGenre'>Choose a Genre:</label>");
        out.println("<select name='pickGenre' id='pickGenre'>");
        out.println("<optgroup label='Genre'>");
        out.println("<option value='9999'>All Genres</option>");

        Map<String, String> genres = this.queryGenres();
        for (Map.Entry<String, String> entry : genres.entrySet()) {
            String id = entry.getKey();
            String name = entry.getValue();
            out.println("<option value='"+ id + "'>" + name + " </option>");

        }

        out.println("</optgroup>");
        out.println("</select>");

        out.println("<label for='pickActor'>........You can also enter the name of an Actor:</label>");
        out.println("<input type='text' name='pickActor' id='pickActor'>");
        out.println("<br><br>");
        out.println("<br><br>");
        out.println("<br><br>");
        out.println("<center><input type='submit' value='Submit'></center>");
        out.println("</form>");
        out.println("</body>");
        out.println("</html>");

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String genreId = request.getParameter("pickGenre").trim();
        String searchName = request.getParameter("pickActor").trim();
        String searchPage = request.getParameter("page");
        int page = 1;
        if (searchPage !=null && !searchPage.isEmpty()) {
            page = Integer.valueOf(searchPage);
        }

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body style='background-color:mediumTurquoise;'>");
        out.println("<img src='https://www.themoviedb.org/assets/2/v4/logos/v2/blue_square_2-d537fb228cf3ded904ef09b136fe3fec72548ebc1fea3fbbd1ad9e36364db38b.svg' alt='The Movie Database " +
                "Logo' " + " style='Float:right' width='115' height='85'>");

        boolean actorError = false;
        Map<String, String> actors = new HashMap<>();
        if (!searchName.isEmpty()) {
            actors = this.queryActors(searchName);
            if (actors.isEmpty()) {
                out.println("<center><h3>There are no actors with that name in the database.</h3></center>");
                actorError = true;
            }
        }

        if (actorError == false) {
            Map<String, String> movies = this.queryMovies(genreId, actors, page);

            if (!movies.isEmpty()) {
                out.println("<h3>Here are some movies you might like:</h3>");
                out.println("<table>");

                out.println("<tr><th></th><th>Movie Title</th><th>Times Watched</th><th>Last Watched</th></tr>");

                for (Map.Entry<String, String> entry : movies.entrySet()) {
                    String id = entry.getKey();
                    String name = entry.getValue();
                    Movie mov = this.getMovie(id);
                    out.println("<tr>");
                    out.println("<td><button onclick=\"window.location.href='Watch_now?id=" +
                            id + "'\">Watch Now</button></td>");
                    out.println("<td>" + name + "</td>");
                    out.println("<td>" + String.valueOf(mov.getTimesWatched()) + "</td>");
                    out.println("<td>" + mov.getLastWatched() + "</td>");
                    out.println("</tr>");
                }

                out.println("</table>");
                String query = "movie-servlet?pickGenre=" + genreId + "&pickActor=" + searchName +
                        "&page=" + String.valueOf(page + 1);
                out.println("<p><form action='" + query + "' method='post'>");
                out.println("<button type='submit'>More Choices</button></form>");
            } else {
                out.println("<center><h3>There are no movies that match your search.</h3></center>");

            }
        }
        out.println("<p><center><a href='movie-servlet'>Search Again</center></a>");
        out.println("<p></body></html>");

    }

    public void destroy() {
    }

    private Movie getMovie(String tmdbId) {
        Session selectSession = MovieSession.getSessionFactory().openSession();
        Query query = selectSession.createQuery("from Movie where tmdb_id='" + tmdbId + "'");
        List<Movie> list = query.getResultList();

        for (Movie mov : list) {
            return mov;
        }
        return new Movie();
    }

    private Map<String, String> queryGenres() {
        Map<String, String> params = new HashMap<>();
        JsonNode genres = (JsonNode) this.queryTmdb("genre/movie/list", params);
        JsonNode array = genres.get("genres");
        Map<String, String> result = new HashMap<>();

        for (int i=0; i< array.size(); i++) {
            JsonNode jsonNode = array.get(i);
            JsonNode id = jsonNode.get("id");
            JsonNode name = jsonNode.get("name");
            result.put(id.asText(), name.asText());
        }
        return result;
    }

    private Map<String, String> queryActors(String searchName) {
        Map<String, String> params = new HashMap<>();
        params.put("query", searchName);
        JsonNode actors = (JsonNode) this.queryTmdb("search/person", params);
        JsonNode array = actors.get("results");
        Map<String, String> result = new HashMap<>();

        if (array !=null && array.size() >0 ) {
            JsonNode jsonNode = array.get(0);
            JsonNode id = jsonNode.get("id");
            JsonNode name = jsonNode.get("name");
            result.put(id.asText(), name.asText());
        }
        return result;
    }

    private Map<String, String> queryMovies(String genreId, Map<String, String> actors, int page) {
        Map<String, String> params = new HashMap<>();

        if (!genreId.equals("9999")) {
            params.put("with_genres", genreId);
        }

        String actorIds = "";

        for (Map.Entry<String, String> entry : actors.entrySet()) {
            actorIds += entry.getKey() + ",";
        }

        if (actorIds !="") {
            params.put("with_people", actorIds);
        }

        if (page >0) {
            params.put("page", String.valueOf(page));
        }

        JsonNode movies = (JsonNode) this.queryTmdb("discover/movie", params);
        JsonNode array = movies.get("results");
        Map<String, String> result = new HashMap<>();

        for (int i=0; i< array.size(); i++) {
            JsonNode jsonNode = array.get(i);
            JsonNode id = jsonNode.get("id");
            JsonNode name = jsonNode.get("title");
            result.put(id.asText(), name.asText());
        }
        return result;
    }

    private Object queryTmdb(String endpoint, Map<String, String> params) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            String some_url = tmdbApiUrl;
            some_url += endpoint;
            some_url += "?";
            some_url += "api_key=" + tmdbApiKey;

            for (Map.Entry<String, String> entry : params.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                value = URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
                some_url += "&";
                some_url += key + "=" + value;
            }

            URLConnection connection = new URL(some_url).openConnection();

            InputStream is = connection.getInputStream();
            StringBuilder textBuilder = new StringBuilder();
            Reader reader = new BufferedReader(new InputStreamReader
                    (is, Charset.forName(StandardCharsets.UTF_8.name())));
            int c = 0;
            while ((c = reader.read()) != -1) {
                textBuilder.append((char) c);
            }

            String json = textBuilder.toString();
            JsonNode jsonNode = objectMapper.readTree(json);
            return jsonNode;

        } catch (Exception e) {
            return objectMapper.createObjectNode();
        }
    }

}