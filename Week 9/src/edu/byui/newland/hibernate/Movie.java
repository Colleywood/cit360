package edu.byui.newland.hibernate;

import javax.persistence.*;

@Entity
@Table(name = "Movie")
class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "movie_title")
    private String movieTitle;

    @Column(name = "movie_year")
    private Integer movieYear;

    @Column(name = "movie_rating")
    private String movieRating;

    @Column(name = "actor_one")
    private String actorOne;

    @Column(name = "actor_two")
    private String actorTwo;

    public Movie() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public Integer getMovieYear() {
        return movieYear;
    }

    public void setMovieYear(Integer movieYear) {
        this.movieYear = movieYear;
    }

    public String getMovieRating() {
        return movieRating;
    }

    public void setMovieRating(String movieRating) {
        this.movieRating = movieRating;
    }

    public String getActorOne() {
        return actorOne;
    }

    public void setActorOne(String actorOne) {
        this.actorOne = actorOne;
    }

    public String getActorTwo() {
        return actorTwo;
    }

    public void setActorTwo(String actorTwo) {
        this.actorTwo = actorTwo;
    }

    public String toString() {
        return Integer.toString(id) + " "
                + movieTitle + " "
                + movieYear + " "
                + movieRating + " "
                + actorOne + " "
                + actorTwo + " ";
    }

}
