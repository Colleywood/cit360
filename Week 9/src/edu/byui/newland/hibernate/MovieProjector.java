package edu.byui.newland.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

public class MovieProjector {

    public static void main(String[] args) {

        Session insertSession = MovieSnackBar.getSessionFactory().openSession();
        insertSession.getTransaction().begin();

        /** Insert movie title */
        Movie nm = new Movie();
        nm.setMovieTitle("Back from the Future");
        nm.setMovieYear(1984);
        nm.setMovieRating("PG6");
        nm.setActorOne("Michael K Fox");
        nm.setActorTwo("Christopher Floyd");

        insertSession.save(nm);
        insertSession.getTransaction().commit();

        System.out.println(nm);

        /** Update movie title */

        Session updateSession = MovieSnackBar.getSessionFactory().openSession();
        updateSession.getTransaction().begin();

        Movie um = new Movie();
        um.setId(nm.getId());
        um.setMovieTitle("Back to the Future");
        um.setMovieYear(1985);
        um.setMovieRating("PG");
        um.setActorOne("Michael J Fox");
        um.setActorTwo("Christopher Lloyd");

        updateSession.update(um);
        updateSession.getTransaction().commit();

        System.out.println(um);

        /** Attempt to select movie title and eventually print out result */
        Session selectSession = MovieSnackBar.getSessionFactory().openSession();
        Query query = selectSession.createQuery("from Movie");
        List<Movie> list = query.getResultList();

        for (Movie mov : list) {
            System.out.println(mov);
        }

        MovieSnackBar.shutdown();
    }
}
