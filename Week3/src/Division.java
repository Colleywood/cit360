import java.util.*;

public class Division {

    public static void main(String[] args) {

        Scanner num = new Scanner(System.in);
        int num1, num2, result;
        num1 = 0;
        num2 = 0;
        result = 0;
        boolean good;

        do {
            System.out.println("Enter your first number ");
            do {
                try {
                    num1 = num.nextInt();
                    good = true;
                } catch (InputMismatchException ex) {
                    num.nextLine();
                    System.out.println("I need a number please.");
                    good = false;
                }
            } while (!good);

            System.out.println("Enter your second number ");
            do {
                try {
                    num2 = num.nextInt();
                    good = true;
                } catch (InputMismatchException ex) {
                    num.nextLine();
                    System.out.println("I need a number please.");
                    good = false;
                }
            } while (!good);

            if (num2 <= 0) {
                System.out.println("Please don't divide by zero or a negative number. Try again.");
            }
        }
        while (num2 <= 0);

        try {
            result = doDivision(num1, num2);
        } catch (ArithmeticException ex) {
            System.out.println("Invalid division occurred.");
        }

        System.out.println("The Integer division result is " + result);
        num.close();
    }
    private static int doDivision ( int num1, int num2) throws ArithmeticException {
        return num1 / num2;
    }
}