package com.example.Week7Servlets;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/order-servlet")
public class OrderResource {
    @GET
    @Produces("text/plain")
    public String order() {
        return "Good day!";
    }
}