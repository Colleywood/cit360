package com.example.Week7Servlets;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;


@WebServlet(name = "order-servlet", urlPatterns =  {"/order-servlet"} )
public class OrderServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Enjoy! We hope you came hungry!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Greeting
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String appetiser = request.getParameter("appetizer").trim();
        String main = request.getParameter("main").trim();
        String dessert = request.getParameter("dessert").trim();
        String order = "You will start with " + appetiser
                + ", followed by a main dish " + main
                + ", and finally, the best part -- the dessert! " + dessert;
        response.setContentType("text/html");

        // Greeting
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("<p>" + order + "</p>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}