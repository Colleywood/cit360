<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Week7</title>
</head>
<body>
<form action="order-servlet" method="post">
    <h1>Welcome to our restaurant!</h1>

    <ul>
        <li>
            Appetizer: <input type="text" name="appetizer" />
        </li>
        <li>
            Main Dish: <input type="text" name="main" />
        </li>
        <li>
            Dessert: <input type="text" name="dessert" />
        </li>
    </ul>
    <input type="submit" value="Order" />
</form>
</body>
</html>