package edu.byui.newland.Week8Threads;

import java.util.Random;

public class RunningMan implements Runnable {

    private int number;
    private String trackTeam;
    private RunningMan previous;
    private int distance;
    private int rand;

    public RunningMan(int number, String trackTeam, RunningMan previous) {
        this.number=number;
        this.trackTeam=trackTeam;
        this.previous=previous;
        this.distance=0;

        Random random = new Random();
        this.rand = random.nextInt(50);

    }

    public void run() {
        //Sprinter waits for previous runner hand-off
        while (this.previous!=null && this.previous.distance<100) {
            try {
                Thread.currentThread().sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //Sprinter runs for 100 meters
        for (this.distance=0; this.distance<100; this.distance++) {
            if (this.distance %10==0) {
                int meters=this.distance+((number-1)*100);
                System.out.println(trackTeam + " track team"
                        + " sprinter " + number
                        + ": " + meters + " m");
            }
            try {
                Thread.currentThread().sleep(50 + rand);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (this.number<4) {
            System.out.println(trackTeam + " track team"
                    + " sprinter " + number
                    + ": Passed the baton...");
        }
        else {
            System.out.println(trackTeam + " track team"
                    + " sprinter " + number
                    + ": Crossed the finish line!");
        }
    }
}
