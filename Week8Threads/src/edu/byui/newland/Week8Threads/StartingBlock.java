package edu.byui.newland.Week8Threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class StartingBlock {

    public static void createTeam(ExecutorService executor, String trackTeam) {
        RunningMan previous=null;

        for (int i=1; i<=4; i++) {
            RunningMan rm = new RunningMan(i, trackTeam, previous);
            executor.execute(rm);
            previous=rm;
        }
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(16);

        //Create the teams
        createTeam(executor, "BYU-I");
        createTeam(executor, "Boise State");
        createTeam(executor, "Utah State");
        createTeam(executor, "UNLV");

        //Shutdown the executor
        try {
            executor.shutdown();
            executor.awaitTermination(100, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            System.err.println("Relay race interrupted.");
        }
        finally {
            if (!executor.isTerminated()) {
                executor.shutdownNow();
            }
            System.out.println("\nIt's over folks. We have a winner!!!");
        }
    }
}
