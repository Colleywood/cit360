package Week4;

public class Hero {

    private int heroId;
    private String firstName;
    private String lastName;
    private String heroName;

    public int getHeroId() {
        return heroId;
    }

    public void setHeroId(int heroId) {
        this.heroId = heroId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHeroName() {
        return heroName;
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    public String toString() {
        return "Hero: " + heroId + " First_Name: " + firstName + " Last_Name: " + lastName + " Hero_Name: " + heroName;
    }
}
