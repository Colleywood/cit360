package Week4;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;

public class HeroesOfJSON {

    public Object str;

    public static void main(String[] args) {
        HeroesOfJSON heroesOfJSON = new HeroesOfJSON();
        heroesOfJSON.heroToJSON();
        heroesOfJSON.JSONToHeroes();
    }

    public Object heroToJSON() {
        System.out.println("Wait here Lois...");
        Hero hero = new Hero();
        hero.setHeroId(1);
        hero.setFirstName("Clark");
        hero.setLastName("Kent");
        hero.setHeroName("Superman");

        ObjectMapper objectMapper = new ObjectMapper(); //creates ObjectMapper instance

        try {
            this.str = objectMapper.writeValueAsString(hero); //generates JSON from Java object as a string
            System.out.println(this.str);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return this.str;
    }

    public Hero JSONToHeroes() {
        String data = (String) this.str;
        System.out.println("Clark, I got this...");
        ObjectMapper objectMapper = new ObjectMapper();
        Hero hero = null;

        try {
            hero = objectMapper.readValue(data, Hero.class);
            System.out.println("First Name: " + hero.getFirstName());
            System.out.println("Last Name: " + hero.getLastName());
            System.out.println("Hero Name: " + hero.getHeroName());
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return hero;
    }
}
